package com.mobaxe.photocleaner.models;

public class Contact {

    public int contactId;
    public int rawContactId;
    public String displayName;
    public String phoneNumber;
    public String thumbUri;
    public String photoUri;
    public boolean isSelected;
    public int adapterPosition;

    @Override
    public String toString() {
        return "Contact{" +
                "contactId=" + contactId +
                ", rawContactId=" + rawContactId +
                ", displayName='" + displayName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", thumbUri='" + thumbUri + '\'' +
                ", photoUri='" + photoUri + '\'' +
                ", isSelected='" + isSelected + '\'' +
                '}';
    }
}
