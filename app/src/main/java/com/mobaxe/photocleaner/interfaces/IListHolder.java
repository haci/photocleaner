package com.mobaxe.photocleaner.interfaces;

import com.joooonho.SelectableRoundedImageView;

public interface IListHolder {

    int getPosition();

    SelectableRoundedImageView getImageView();

}
