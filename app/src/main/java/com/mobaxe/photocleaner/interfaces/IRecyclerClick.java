package com.mobaxe.photocleaner.interfaces;

import com.joooonho.SelectableRoundedImageView;

public interface IRecyclerClick {

    void onItemClicked(int row, SelectableRoundedImageView contactImage);

}
