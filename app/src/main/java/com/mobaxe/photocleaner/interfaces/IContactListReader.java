package com.mobaxe.photocleaner.interfaces;

import com.mobaxe.photocleaner.models.Contact;

import java.util.List;

public interface IContactListReader {
    void onContactsRead(List<Contact> contacts);
}
