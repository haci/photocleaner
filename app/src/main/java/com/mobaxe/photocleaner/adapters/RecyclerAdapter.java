package com.mobaxe.photocleaner.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.mobaxe.photocleaner.R;
import com.mobaxe.photocleaner.interfaces.IListHolder;
import com.mobaxe.photocleaner.interfaces.IRecyclerClick;
import com.mobaxe.photocleaner.models.Contact;
import com.mobaxe.photocleaner.utils.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {
    ImageLoader imageLoader;
    private IRecyclerClick recyclerClick;

    private List<Contact> mData;
    private List<Contact> mDummyData;

    private String queryText;

    public RecyclerAdapter(List<Contact> contacts, ImageLoader imageLoader) {
        this.mData = contacts;
        this.mDummyData = contacts;
        this.imageLoader = imageLoader;
    }

    public void setOnClickListener(IRecyclerClick recyclerClick) {
        this.recyclerClick = recyclerClick;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row, parent, false);
        return new RecyclerViewHolder(v);
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements IListHolder, View.OnClickListener {

        SelectableRoundedImageView contactImageView;
        TextView contactNameTxt;
        TextView contactPhoneTxt;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            contactImageView = (SelectableRoundedImageView) itemView.findViewById(R.id.contact_image);
            contactNameTxt = (TextView) itemView.findViewById(R.id.contact_name);
            contactPhoneTxt = (TextView) itemView.findViewById(R.id.contact_phone);

            itemView.setTag(contactImageView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            recyclerClick.onItemClicked(getLayoutPosition(), (SelectableRoundedImageView) v.getTag());
        }

        @Override
        public SelectableRoundedImageView getImageView() {
            return contactImageView;
        }
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.getImageView().clearAnimation();
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int rowPosition) {

        Contact c = mDummyData.get(rowPosition);
        c.adapterPosition = rowPosition;
        
        imageLoader.displayImage(c.photoUri, c.thumbUri, holder, rowPosition);

        try {

            if (!TextUtils.isEmpty(queryText) && queryText.length() > 1) {

                String displayName = " " + c.displayName;
                Pattern p = Pattern.compile(queryText, Pattern.CASE_INSENSITIVE);
                if (p.matcher(displayName).find()) {

                    int start = indexOf(displayName, false, queryText);

                    if (start == -1) {
                        start = 0;
                    }
                    int end = queryText.length() + start;
                    if (end > 0) {
                        end = end - 1;
                    }

                    Spanned spanned = Html.fromHtml(c.displayName.substring(0, start) +
                            "<font color='#03A9F5'>" + c.displayName.substring(start, end) + "</font>" +
                            c.displayName.substring(end, c.displayName.length()));

                    holder.contactNameTxt.setText(spanned);
                } else {
                    holder.contactNameTxt.setText(c.displayName);
                }
            } else {
                holder.contactNameTxt.setText(c.displayName);
            }
        } catch (Exception e) {
            holder.contactNameTxt.setText(c.displayName);
        }
        holder.contactPhoneTxt.setText(c.phoneNumber);

        if (c.isSelected) {
            holder.contactImageView.setAlpha(.5f);
        } else {
            holder.contactImageView.setAlpha(1f);
        }

    }

    @Override
    public int getItemCount() {
        return mDummyData.size();
    }

    public void filter(CharSequence constraint) {

        List<Contact> contactList = new ArrayList<>();

        if (!constraint.equals(" ")) {
            String constraintBlank = " " + constraint;
            Pattern p = Pattern.compile(constraintBlank, Pattern.CASE_INSENSITIVE);
            for (Contact c : mData) {
                String displayName = " " + c.displayName;

                if (p.matcher(displayName).find()) {
                    contactList.add(c);
                }
            }
            queryText = constraintBlank;
            mDummyData = contactList;
            notifyDataSetChanged();
        } else {
            queryText = constraint.toString();
            mDummyData = contactList;
            notifyDataSetChanged();
        }
    }

    public void setDummyContacts(List<Contact> contacts) {
        this.mDummyData = contacts;
        notifyDataSetChanged();
        System.out.println("DuMMY CONTACTS SETl");
    }

    public int getContactsCountToDelete() {
        int count = 0;
        for (Contact c : mDummyData) {
            if (c.isSelected)
                count++;
        }
        return count;
    }

    public int indexOf(String mainStr, boolean caseSensitive, String toFind) {
        int toFindIndex = 0;
        char l = toFind.charAt(toFindIndex);
        if (!caseSensitive) {
            l = Character.toLowerCase(l);
        }
        for (int i = 0; i < mainStr.length(); i++) {
            char c = mainStr.charAt(i);
            if (!caseSensitive) {
                c = Character.toLowerCase(c);
            }
            if (c != l) {
                toFindIndex = 0;
            } else if (toFindIndex + 1 == toFind.length()) {
                return i - toFind.length() + 1;
            } else {
                ++toFindIndex;
            }
            l = toFind.charAt(toFindIndex);
            if (!caseSensitive) {
                l = Character.toLowerCase(l);
            }
        }
        return -1;
    }

}