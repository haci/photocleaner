package com.mobaxe.photocleaner.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


import com.mobaxe.photocleaner.R;
import com.mobaxe.photocleaner.interfaces.IListHolder;

import java.io.FileDescriptor;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageLoader {
    MemoryCache memoryCache = new MemoryCache();
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    ExecutorService executorService;
    Handler handler;// handler to display images in UI thread
    Context ctx;

    ContentResolver cr;
    private int defaultImage = 0;
    private static final int FADE_IN_TIME = 200;
    private Resources mResources;

    private static ImageLoader mInstance;

    public static ImageLoader getInstance(Context context) {
        if (mInstance == null) {
            synchronized (ImageLoader.class) {
                if (mInstance == null) {
                    mInstance = new ImageLoader(context);
                }
            }
        }
        return mInstance;
    }


    public ImageLoader(Context context) {
        handler = new Handler();
        ctx = context;
        executorService = Executors.newFixedThreadPool(5);

        cr = ctx.getContentResolver();
        mResources = context.getResources();
    }

    public void displayImage(String photoUrl, String thumbnailUrl, IListHolder listHolder, int position) {
        if (TextUtils.isEmpty(photoUrl) & TextUtils.isEmpty(thumbnailUrl)) {
            listHolder.getImageView().setImageResource(defaultImage);
            return;
        }
        ImageView imageView = listHolder.getImageView();

        imageViews.put(imageView, thumbnailUrl);
        Bitmap bitmap = memoryCache.get(thumbnailUrl);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
//            imageView.startAnimation(AnimationUtils.loadAnimation(imageView.getContext(),
//                    R.anim.fade));
        } else {
            queuePhoto(photoUrl, thumbnailUrl, listHolder, position);
            imageView.setImageDrawable(null);
        }
    }

    private void queuePhoto(String photoUrl, String thumbUrl, IListHolder listHolder, int position) {
        PhotoToLoad p = new PhotoToLoad(photoUrl, thumbUrl, listHolder, position);
        executorService.submit(new PhotosLoader(p));
    }

    private Bitmap getBitmap(String url) {
        //Bitmap photo = null;
        try {
            //  return ContactListManager.retrieveContactPhoto(url, cr, true);
            Uri myUri = Uri.parse(url);
            InputStream input = cr.openInputStream(myUri);
            if (input == null) {
                return null;
            }
            // return decodeScaledBitmapFromInputStream(myUri, input);
            return BitmapFactory.decodeStream(input);

        } catch (Throwable ex) {
            if (ex instanceof OutOfMemoryError) {
                memoryCache.clear();
                return getBitmap(url);
            }

            return null;
        }
    }

    /**
     * Decode and sample down a bitmap from a file input stream to the requested width and height.
     *
     * @param fileDescriptor The file descriptor to read from
     * @param reqWidth       The requested width of the resulting bitmap
     * @param reqHeight      The requested height of the resulting bitmap
     * @return A bitmap sampled down from the original with the same aspect ratio and dimensions
     * that are equal to or greater than the requested width and height
     */
    public static Bitmap decodeSampledBitmapFromDescriptor(
            FileDescriptor fileDescriptor, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
    }

    /**
     * Calculate an inSampleSize for use in a {@link BitmapFactory.Options} object when decoding
     * bitmaps using the decode* methods from {@link BitmapFactory}. This implementation calculates
     * the closest inSampleSize that will result in the final decoded bitmap having a width and
     * height equal to or larger than the requested width and height. This implementation does not
     * ensure a power of 2 is returned for inSampleSize which can be faster when decoding but
     * results in a larger bitmap which isn't as useful for caching purposes.
     *
     * @param options   An options object with out* params already populated (run through a decode*
     *                  method with inJustDecodeBounds==true
     * @param reqWidth  The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return The value to be used for inSampleSize
     */
    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    //    private Bitmap decodeScaledBitmapFromInputStream(Uri myUri, InputStream input) {
//        try {
//            // First decode with inJustDecodeBounds=true to check dimensions
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inJustDecodeBounds = true;
//            BitmapFactory.decodeStream(input, null, options);
//            //BitmapFactory.decodeFile(filePath, options);
//
//            // Calculate inSampleSize
//            options.inSampleSize = calculateInSampleSize(options, iImageWidth, iImageHeight);
//
//            // Decode bitmap with inSampleSize set
//            options.inJustDecodeBounds = false;
//            input = cr.openInputStream(myUri);
//            return BitmapFactory.decodeStream(input, null, options);
//        } catch (Throwable ex) {
//            return null;
//        }
//    }
//
//    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
//        if (iImageWidth == 0) {
//            iImageWidth = ScreenWidth / 2;
//        }
//
//        if (iImageHeight == 0) {
//            iImageHeight = ScreenHeight / 2;
//        }
//
//
//        // Raw height and width of image
//        final int height = options.outHeight;
//        final int width = options.outWidth;
//        int inSampleSize = 1;
//
//        if (height > reqHeight || width > reqWidth) {
//
//            // Calculate ratios of height and width to requested height and
//            // width
//            final int heightRatio = Math.round((float) height / (float) reqHeight);
//            final int widthRatio = Math.round((float) width / (float) reqWidth);
//
//            // Choose the smallest ratio as inSampleSize value, this will
//            // guarantee
//            // a final image with both dimensions larger than or equal to the
//            // requested height and width.
//            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
//        }
//
//        return inSampleSize;
//    }
//
//
    // Task for the queue
    private class PhotoToLoad {
        public String photoUrl;
        public String thumbUrl;
        public IListHolder listHolder;
        private int position;

        public PhotoToLoad(String _photoUrl, String _thumbUrl, IListHolder _listHolder, int _position) {
            photoUrl = _photoUrl;
            thumbUrl = _thumbUrl;
            listHolder = _listHolder;
            position = _position;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            try {
                if (imageViewReused(photoToLoad))
                    return;


                Bitmap bmp = getBitmap(photoToLoad.photoUrl);
                if (bmp != null) {
                    memoryCache.put(photoToLoad.thumbUrl, bmp);
                    if (imageViewReused(photoToLoad))
                        return;

                    BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
                    handler.post(bd);
                } else {
                    bmp = getBitmap(photoToLoad.thumbUrl);
                    if (bmp != null) {
                        memoryCache.put(photoToLoad.thumbUrl, bmp);
                        if (imageViewReused(photoToLoad))
                            return;

                        BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
                        handler.post(bd);
                    } else {
                        photoToLoad.listHolder.getImageView().setImageResource(defaultImage);
                    }
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.listHolder.getImageView());
        if (tag == null || !tag.equals(photoToLoad.thumbUrl))
            return true;

        return false;
    }

    // Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;

            if (bitmap != null) {
                if (photoToLoad.position == photoToLoad.listHolder.getPosition()) {
                    photoToLoad.listHolder.getImageView().setImageBitmap(bitmap);
                    photoToLoad.listHolder.getImageView().startAnimation(AnimationUtils.loadAnimation(photoToLoad.listHolder.getImageView().getContext(),
                            R.anim.fade));
                    //setImageBitmap(photoToLoad.listHolder.getImageView(), bitmap);
                }
                //setImageBitmap(photoToLoad.listHolder.getImageView(), bitmap);
            } else {
                photoToLoad.listHolder.getImageView().setImageResource(defaultImage);
            }
        }
    }

    private void setImageBitmap(ImageView imageView, Bitmap bitmap) {
        boolean mFadeInBitmap = true;
        if (mFadeInBitmap) {
            // Transition drawable to fade from loading bitmap to final bitmap
            final TransitionDrawable td =
                    new TransitionDrawable(new Drawable[]{
                            new ColorDrawable(ContextCompat.getColor(imageView.getContext(), android.R.color.transparent)),
                            new BitmapDrawable(mResources, bitmap)
                    });
            //noinspection deprecation
            imageView.setBackgroundDrawable(imageView.getDrawable());
            imageView.setImageDrawable(td);
            td.startTransition(FADE_IN_TIME);
        } else {
            imageView.setImageBitmap(bitmap);
        }
    }

    public void clearCache() {
        memoryCache.clear();
    }
}
