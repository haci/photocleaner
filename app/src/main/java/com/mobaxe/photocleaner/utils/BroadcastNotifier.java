package com.mobaxe.photocleaner.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

public class BroadcastNotifier {

    public static final String EXTENDED_DATA_STATUS = "com.mobaxe.photocleaner.STATUS";
    public static final String BROADCAST_ACTION = "com.mobaxe.photocleaner.BROADCAST";

    public static final int MENU_CREATED = 123;


    private LocalBroadcastManager mBroadcaster;
    Context ctx;

    public BroadcastNotifier(Context context) {
        ctx = context;
        mBroadcaster = LocalBroadcastManager.getInstance(ctx);
    }

    public void broadcastIntentWithState(int status) {

        Intent localIntent = new Intent();

        localIntent.setAction(BROADCAST_ACTION);
        localIntent.putExtra(EXTENDED_DATA_STATUS, status);
        localIntent.addCategory(Intent.CATEGORY_DEFAULT);

        mBroadcaster.sendBroadcast(localIntent);
    }
}
