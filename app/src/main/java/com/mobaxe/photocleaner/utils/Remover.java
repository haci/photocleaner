package com.mobaxe.photocleaner.utils;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDiskIOException;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;

import com.mobaxe.photocleaner.interfaces.IContactListReader;
import com.mobaxe.photocleaner.models.Contact;

import java.util.ArrayList;
import java.util.List;

public class Remover {

    private ContentResolver resolver;
    private final static byte[] nullBytes = new byte[0];
    private IContactListReader reader;

    public Remover(Context context, IContactListReader reader) {
        this.resolver = context.getContentResolver();
        this.reader = reader;
    }

    public void searchContacts() {
        final List<Contact> contactList = new ArrayList<>();
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {

                String[] contactProjection = new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID,
                        ContactsContract.CommonDataKinds.Phone.PHOTO_URI,
                        ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI,

                };

                String where = ContactsContract.Contacts.HAS_PHONE_NUMBER + " > " + 0 + " AND "
                        + ContactsContract.Contacts.Data.MIMETYPE + "='" + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "'" + " AND "
                        + ContactsContract.RawContacts.ACCOUNT_TYPE + "<>'com.whatsapp'" + " AND "
                        + ContactsContract.RawContacts.ACCOUNT_TYPE + "<>'com.viber.voip'" + " AND "
                        + ContactsContract.RawContacts.ACCOUNT_TYPE + "<>'jp.naver.line.android'" + " AND "
                        + ContactsContract.RawContacts.ACCOUNT_TYPE + "<>'com.skype.raider'" + " AND "
                        + ContactsContract.RawContacts.ACCOUNT_TYPE + "<>'com.linkedin.android'";


                String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";


                Cursor cursor = null;
                try {
                    cursor = resolver.query(ContactsContract.Data.CONTENT_URI, contactProjection, where, null, sortOrder);
                    if (cursor == null) {
                        return null;
                    }
                    int contactIdIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
                    int rawContactIdIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID);
                    int nameIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                    int phoneNumberIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    int photoUriIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);
                    int thumbUriIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI);

                    cursor.moveToFirst();

                    do {
                        int contactId = cursor.getInt(contactIdIdx);
                        String displayName = cursor.getString(nameIdx);
                        String originalPhone = cursor.getString(phoneNumberIdx);
                        int rawContactId = cursor.getInt(rawContactIdIdx);
                        String thumbUri = cursor.getString(thumbUriIdx);
                        String photoUri = cursor.getString(photoUriIdx);

                        Contact contact = new Contact();
                        contact.contactId = contactId;
                        contact.rawContactId = rawContactId;
                        contact.displayName = displayName;
                        contact.phoneNumber = originalPhone;
                        contact.thumbUri = thumbUri;
                        contact.photoUri = photoUri;


                        if (!TextUtils.isEmpty(contact.thumbUri) || !TextUtils.isEmpty(contact.photoUri))
                            contactList.add(contact);

                    } while (cursor.moveToNext());

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                reader.onContactsRead(contactList);

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public boolean remove(int rawContactId) {

        ContentValues values = new ContentValues();

        int photoRow = -1;
        String where = ContactsContract.Data.RAW_CONTACT_ID + " == " +
                rawContactId + " AND " + ContactsContract.Data.MIMETYPE + "=='" +
                ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE + "'";

        Cursor cursor = resolver.query(ContactsContract.Data.CONTENT_URI, null, where, null, null);
        if (cursor == null) {
            return false;
        }
        int idIdx = cursor.getColumnIndexOrThrow(ContactsContract.Data._ID);
        if (cursor.moveToFirst()) {
            photoRow = cursor.getInt(idIdx);
        }
        cursor.close();
        values.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
        values.put(ContactsContract.CommonDataKinds.Photo.PHOTO, nullBytes);
        values.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE);
        try {
            if (photoRow >= 0) {
                resolver.update(ContactsContract.Data.CONTENT_URI, values, ContactsContract.Data._ID + " = " + photoRow, null);
                System.out.println("UPDATE RAW ID : " + rawContactId);
            } else {
                resolver.insert(ContactsContract.Data.CONTENT_URI, values);
                System.out.println("INSERT RAW ID : " + rawContactId);
            }
            return true;
        } catch (SQLiteDiskIOException dIOe) {
            dIOe.printStackTrace();
        }
        return false;
    }

}
