package com.mobaxe.photocleaner.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class PermissionRequester {

    private static final int REQUEST_CONTACTS = 153;
    private final static String[] NEEDED_PERMISSIONS_ARRAY = {Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS};


    public static int checkSelfPermissions(Context context) {
        List<String> neededRequests = new ArrayList<>();

        for (int i = 0; i < NEEDED_PERMISSIONS_ARRAY.length; i++) {
            if (ContextCompat.checkSelfPermission(context, NEEDED_PERMISSIONS_ARRAY[i]) != PackageManager.PERMISSION_GRANTED) {
                neededRequests.add(NEEDED_PERMISSIONS_ARRAY[i]);
            }
        }
        return neededRequests.size();
    }

    public static int requestPermissions(Context context) {

        int neededRequestSize = checkSelfPermissions(context);

        if (neededRequestSize == NEEDED_PERMISSIONS_ARRAY.length) {
            ActivityCompat.requestPermissions((AppCompatActivity) context, NEEDED_PERMISSIONS_ARRAY,
                    REQUEST_CONTACTS);
        }
        return neededRequestSize;
    }
}
