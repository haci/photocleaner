package com.mobaxe.photocleaner.activities;

import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.github.clans.fab.FloatingActionButton;
import com.joooonho.SelectableRoundedImageView;
import com.mobaxe.photocleaner.R;
import com.mobaxe.photocleaner.adapters.RecyclerAdapter;
import com.mobaxe.photocleaner.interfaces.IContactListReader;
import com.mobaxe.photocleaner.interfaces.IRecyclerClick;
import com.mobaxe.photocleaner.models.Contact;
import com.mobaxe.photocleaner.utils.BroadcastNotifier;
import com.mobaxe.photocleaner.utils.ImageLoader;
import com.mobaxe.photocleaner.utils.PermissionRequester;
import com.mobaxe.photocleaner.utils.Remover;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IContactListReader, SearchView.OnQueryTextListener {

    private List<Contact> contactList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RelativeLayout noPermissLay;
    private Button openAppDetailsBtn;
    RecyclerAdapter recyclerAdapter;
    MenuCreateReceiver brodcastReceiver;
    FloatingActionButton fab;
    Remover remover;

    private final static int PERMISSIONS_REQUEST_CODE = 211;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        noPermissLay = (RelativeLayout) findViewById(R.id.no_permission_layout);
        openAppDetailsBtn = (Button) findViewById(R.id.app_details_btn);

        remover = new Remover(this, this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide(false);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recyclerAdapter != null && recyclerAdapter.getContactsCountToDelete() > 0) {
                    fab.setEnabled(false);
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            for (int i = 0; i < contactList.size(); i++) {
                                Contact c = contactList.get(i);
                                if (c.isSelected) {
                                    boolean isRemoved = remover.remove(c.rawContactId);
                                    if (isRemoved) {
                                        contactList.remove(i);
                                        i--;
                                    }
                                }
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            fab.setEnabled(true);
                            if (recyclerAdapter != null) {
                                recyclerAdapter.notifyDataSetChanged();
                            }
                            super.onPostExecute(aVoid);
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int neededRequestSize = PermissionRequester.requestPermissions(this);
            if (neededRequestSize == 0) {
                runApp();
            }
        } else {
            runApp();
        }

        brodcastReceiver = new MenuCreateReceiver();
        IntentFilter intentFilter = new IntentFilter(BroadcastNotifier.BROADCAST_ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        LocalBroadcastManager.getInstance(this).registerReceiver(brodcastReceiver, intentFilter);
    }

    @Override
    public void onContactsRead(List<Contact> contacts) {
        contactList.clear();
        contactList.addAll(contacts);


        if (contactList.size() > 0) {
            GridLayoutManager glm = new GridLayoutManager(this, 2);
            recyclerView.setLayoutManager(glm);
            ImageLoader loader = new ImageLoader(this);
            recyclerAdapter = new RecyclerAdapter(contactList, loader);
            recyclerAdapter.setOnClickListener(new IRecyclerClick() {
                @Override
                public void onItemClicked(int row, SelectableRoundedImageView contactImage) {
                    Contact c = contactList.get(row);
                    if (c.isSelected) {
                        c.isSelected = false;
                        contactImage.setAlpha(1f);
                    } else {
                        c.isSelected = true;
                        contactImage.setAlpha(.5f);
                    }
                    if (fab.isHidden()) {
                        int count = recyclerAdapter.getContactsCountToDelete();
                        if (count > 0)
                            fab.show(true);
                    } else {
                        int count = recyclerAdapter.getContactsCountToDelete();
                        if (count == 0)
                            fab.hide(true);
                    }
                }
            });
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(recyclerAdapter);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setVisibility(View.VISIBLE);
            if (menuSearch != null) {
                menuSearch.setVisible(true);
            }
        } else {
            findViewById(R.id.no_contacts_txt_view).setVisibility(View.VISIBLE);
        }

    }

    private void runApp() {
        remover.searchContacts();
    }

    private void openAppDetails() {
        String packageName = getApplicationContext().getPackageName();

        try {
            Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:" + packageName));
            startActivityForResult(intent, PERMISSIONS_REQUEST_CODE);

        } catch (ActivityNotFoundException e) {
            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
            startActivity(intent);
        }
    }

    @Override
    public void onDestroy() {
        try {
            if (brodcastReceiver != null) {
                LocalBroadcastManager.getInstance(this).unregisterReceiver(brodcastReceiver);
                brodcastReceiver = null;
            }
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            int neededRequestSize = PermissionRequester.checkSelfPermissions(MainActivity.this);
            if (neededRequestSize == 0) {
                if (noPermissLay.getVisibility() == View.VISIBLE) {
                    noPermissLay.setVisibility(View.GONE);
                    runApp();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int grantedPermissions = 0;
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                grantedPermissions++;
            }
        }
        if (grantedPermissions == permissions.length) {
            if (noPermissLay.getVisibility() == View.VISIBLE) {
                noPermissLay.setVisibility(View.GONE);
            }
            runApp();
        } else {
            showNoPermissionView();
        }

    }

    private void showNoPermissionView() {
        if (noPermissLay.getVisibility() == View.GONE) {
            noPermissLay.setVisibility(View.VISIBLE);
        }

        openAppDetailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAppDetails();
            }
        });
    }

    SearchView searchView;
    MenuItem menuSearch;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        try {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_main, menu);

            menuSearch = menu.findItem(R.id.action_search);

            SearchManager searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);

            searchView = (SearchView) menuSearch.getActionView();
            searchView.setOnQueryTextListener(this);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
            searchView = (SearchView) MenuItemCompat.getActionView(menuSearch);
            menuSearch.setVisible(false);
            BroadcastNotifier notifier = new BroadcastNotifier(getApplicationContext());
            notifier.broadcastIntentWithState(BroadcastNotifier.MENU_CREATED);
        } catch (Exception e) {
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                Intent i = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (recyclerAdapter == null)
            return false;
        if (!TextUtils.isEmpty(query)) {
            recyclerAdapter.filter(query);
        }

        return true;
    }

    int runCount = 0;

    @Override
    public boolean onQueryTextChange(String newText) {
        if (recyclerAdapter == null)
            return false;
        if (!TextUtils.isEmpty(newText)) {
            recyclerAdapter.filter(newText);
        } else {
            runCount++;
            if (runCount > 1) {
                recyclerAdapter.filter(" ");
                recyclerAdapter.setDummyContacts(contactList);
            }
        }
        return true;
    }

    private class MenuCreateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int iState = intent.getIntExtra(BroadcastNotifier.EXTENDED_DATA_STATUS, -1);
            if (iState == BroadcastNotifier.MENU_CREATED) {
                if (contactList.size() > 0) {
                    if (menuSearch != null)
                        menuSearch.setVisible(true);
                }
            }
        }
    }

}
