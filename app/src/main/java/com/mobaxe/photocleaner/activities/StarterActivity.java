package com.mobaxe.photocleaner.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mobaxe.photocleaner.R;

public class StarterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starter);

        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();

    }

}
